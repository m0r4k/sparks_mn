#!/bin/bash

#wget https://github.com/SparksReborn/sparkspay/releases/download/v0.12.3.1/sparkscore-0.12.3.1-linux64.tar.gz

test_dir=".spks"
update_dir="sparkscore-0.12.3"
old_conf_dir=".Sparks"
new_conf_dir=".sparkscore"

git clone https://github.com/SparksReborn/sentinel.git
cd sentinel
virtualenv ./venv
./venv/bin/pip install -r requirements.txt
cd .. 
mv sentinel /tmp

wget https://github.com/SparksReborn/sparkspay/releases/download/bootstrap/bootstrap.dat
mv bootstrap.dat /tmp/

if [ ! -f "sparkscore-0.12.3.1-linux64.tar.gz" ];
then
	wget https://github.com/SparksReborn/sparkspay/releases/download/v0.12.3.1/sparkscore-0.12.3.1-linux64.tar.gz
	tar xvzf sparkscore*.gz
fi

if pgrep -x "Sparksd" > /dev/null
then
    daemon_name=$(ls /etc/systemd/system/ | grep sparks)
    if echo "$daemon_name" | grep -iq "^sparks" ;then
      # sudo systemctl stop $daemon_name
      for i in $(ls /etc/systemd/system/ | grep sparks | cut -d "." -f 1) ; do sudo systemctl stop $i ; done
      echo found
   else
     # Sparks-cli stop
     echo "$daemon_name"
 	echo found
   fi
fi


sudo cp $update_dir/bin/* /usr/local/bin/


for i in $(ls /etc/systemd/system/ | grep sparks);
do
	sudo sed -i.bak -e "s/\.Sparks/\.sparkscore/g"  -e "s/Sparksd/sparksd/g" -e "s/Sparks.conf/sparks.conf/g" /etc/systemd/system/$i
	#echo $i
done

sudo systemctl daemon-reload


for i in $(sudo find /home/ | grep "/.Sparks/Sparks.conf" | cut -d/ -f3) 
do 
	echo $i
	sudo mkdir /home/$i/$new_conf_dir 
	sudo cp /home/$i/$old_conf_dir/Sparks.conf /home/$i/$new_conf_dir/sparks.conf
	sudo cp /home/$i/$old_conf_dir/masternode.conf /home/$i/$new_conf_dir/
	sudo cp /home/$i/$old_conf_dir/wallet.dat /home/$i/$new_conf_dir/
	sudo sed -i.bak '/addnode/d' /home/$i/$new_conf_dir/sparks.conf
	sudo cp /tmp/bootstrap.dat /home/$i/$new_conf_dir/
	echo done
	#sudo rm -R /home/$i/$test_dir

	echo now sentinel
	sudo cp -R /tmp/sentinel /home/$i/$new_conf_dir/
	sudo -i -u $i /bin/bash -l -c  "crontab -l | sed "s/Sparks/sparkscore/g" | crontab"
	sudo chown -R $i:$i /home/$i/$new_conf_dir
done


for i in $(ls /etc/systemd/system/ | grep sparks | grep -v -e "bak" | cut -d "." -f1);
do
	sudo systemctl start $i
done




