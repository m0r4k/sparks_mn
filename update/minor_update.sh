#!/bin/bash

#wget https://github.com/SparksReborn/sparkspay/releases/download/v0.12.3.1/sparkscore-0.12.3.1-linux64.tar.gz

test_dir=".spks"
update_dir="./tmp/sparkscore-0.12.3"
old_conf_dir=".Sparks"
new_conf_dir=".sparkscore"

if [ ! -f "./tmp/sparkscore-0.12.3.2-linux64.tar.gz" ];
then
	wget https://github.com/SparksReborn/sparkspay/releases/download/v0.12.3.2/sparkscore-0.12.3.2-linux64.tar.gz -P ./tmp
	mkdir tmp
	tar xvzf ./tmp/sparkscore*.gz -C tmp
fi

if pgrep -x "sparksd" > /dev/null
then
    daemon_name=$(ls /etc/systemd/system/ | grep sparks)
    if echo "$daemon_name" | grep -iq "^sparks" ;then
      # sudo systemctl stop $daemon_name
      for i in $(ls /etc/systemd/system/ | grep sparks | cut -d "." -f 1) ; do sudo systemctl stop $i ; done
      echo found
   else
     # Sparks-cli stop
     echo "$daemon_name"
 	echo found
   fi
fi


sudo cp $update_dir/bin/* /usr/local/bin/


for i in $(ls /etc/systemd/system/ | grep sparks | grep -v -e "bak" | cut -d "." -f1);
do
	sudo systemctl start $i
done

rm -R tmp


